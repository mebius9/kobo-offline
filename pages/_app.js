import '../styles/index.css'
import '../styles/layout-structure.css'
import '../styles/general.css'
import '../styles/code-blocks.css'
import '../styles/sectioning.css'
import '../styles/downloads-list.css'
import '../styles/card.css'

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
